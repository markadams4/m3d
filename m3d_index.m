% get index set for equations for one of 3 fields [idx: 0,1,2] in
% M3D momemtnum kequation
%  
% N: number of vertices
% idx: field index (0:U ; 1:omega ; 2:Xi)
% num_feilds: 1-3 (U, omega, Xi)
% num_subfields12: = 12
%
function index = m3d_index(N, idx, num_fields, num_subfields12)
    num_subfields12 = 12
    if nargin <= 2
        num_fields = 3
    end
    index = kron(0:num_subfields12:num_subfields12*N-1,num_fields*ones(1,num_subfields12)) + kron(ones(1,N),1:num_subfields12) + num_subfields12*idx;
end

