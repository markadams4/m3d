static char help[] = "Reads a PETSc matrix and vector from a file and solves a linear system.\n\
Input arguments are:\n\
  -f <input_file> : file to load.  For example see $PETSC_DIR/share/petsc/datafiles/matrices\n\n";

#include <petscmat.h>
#include <petscksp.h>

int main(int argc, char **args)
{
  PetscInt      *idx[3], its, m, n, iStart,iEnd,dofPerEnt, num_fields, active_field;
  const PetscInt stride = 12;
  PetscReal     norm, normb, xiscale = -1e-9;
  Vec           x, b, u, subx, subb;
  Mat           A, matSub[3], subA;
  KSP           ksp;
  char          file[PETSC_MAX_PATH_LEN];
  PetscViewer   fd;
  PetscLogStage stage1;
  PetscBool     flg;
  const PetscInt *values;
  IS field[3], is;

  PetscFunctionBeginUser;
  PetscCall(PetscInitialize(&argc, &args, (char *)0, help));

  /* Read matrix and RHS */
  PetscCall(PetscOptionsGetString(NULL, NULL, "-f", file, sizeof(file), &flg));
  if (flg) PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Have file name %s\n", file));
  else return 0;
  PetscCall(PetscViewerBinaryOpen(PETSC_COMM_WORLD, file, FILE_MODE_READ, &fd));
  PetscCall(MatCreate(PETSC_COMM_WORLD, &A));
  PetscCall(MatSetType(A, MATAIJ));
  PetscCall(MatSetFromOptions(A));
  PetscCall(MatLoad(A, fd));
  PetscCall(MatGetBlockSize(A, &dofPerEnt));
  PetscCall(MatGetSize(A, &n, NULL));
  PetscCheck(n%dofPerEnt == 0 && dofPerEnt > 1, PETSC_COMM_SELF, PETSC_ERR_PLIB, "bs = %" PetscInt_FMT " n.mode_bs != 0 (use -mat_block_size 36)", dofPerEnt);
  PetscCall(MatCreateVecs(A, &x, &b));
  PetscCall(VecLoad(b, fd));
  PetscCall(VecLoad(x, fd));
  PetscCall(PetscViewerDestroy(&fd));
  PetscCall(MatGetOwnershipRange(A, &iStart, &iEnd));
  num_fields = dofPerEnt/stride; //U 0->11, Omega 12->23, Chi 24->35
  PetscCheck(num_fields <= 3 && num_fields > 0, PETSC_COMM_SELF, PETSC_ERR_PLIB, "num_fields %" PetscInt_FMT " > 3", num_fields);
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Matrix block size %d, N = %d\n", dofPerEnt, n));
  // the 3 fields for PCFIELDSPLIT
  const PetscInt num_own_dof = iEnd - iStart, num_own_ent = num_own_dof/dofPerEnt, istart_ent = iStart/stride;
  PetscCall(PetscMalloc3(num_own_ent, &idx[0], num_own_ent, &idx[1], num_own_ent, &idx[2]));
  for (int v=0; v<num_own_ent; v++) {
    for (int ii = 0 ; ii < num_fields ; ii++) idx[ii][v] = istart_ent + num_fields*v + ii;
  }
  for (int ii = 0 ; ii < num_fields ; ii++) {
    PetscCall(ISCreateBlock(PETSC_COMM_WORLD, stride, num_own_ent, idx[ii], PETSC_COPY_VALUES, &field[ii]));
  }
  for (int ii = 0 ; ii < num_fields ; ii++) {
    PetscCall(ISGetIndices(field[ii], &values));
    PetscCall(ISCreateGeneral(PETSC_COMM_WORLD, stride*num_own_ent, values, PETSC_COPY_VALUES, &is));
    PetscCall(ISRestoreIndices(field[ii], &values));
    PetscCall(MatCreateSubMatrix(A, is, is, MAT_INITIAL_MATRIX, &matSub[ii]));
    PetscCall(ISDestroy(&is));
    PetscCall(MatSetBlockSize(matSub[ii], stride));
    PetscCall(MatGetSize(matSub[ii], &n, &m));
    PetscCall(PetscPrintf(PETSC_COMM_WORLD, "mat %d size %d, %d\n", ii, n, m));
  }
  //PetscCall(ISView(is, PETSC_VIEWER_STDOUT_WORLD));
  if (num_fields == 3) PetscCall(MatScale(matSub[2], xiscale));
  if (num_fields == 3) active_field = 1; // field1
  else active_field = num_fields - 1;
  PetscCall(PetscOptionsGetInt(NULL, NULL, "-active_field", &active_field, &flg));
  subA = matSub[active_field];
  PetscCall(VecGetSubVector(b, field[active_field], &subb));
  PetscCall(VecViewFromOptions(subb, NULL, "-sub_vec_view"));
  PetscCall(VecGetSubVector(x, field[active_field], &subx));
  PetscCall(MatViewFromOptions(subA, NULL, "-sub_mat_view"));
  PetscCall(MatViewFromOptions(A, NULL, "-hole_mat_view"));
  PetscCall(MatNorm(subA, NORM_FROBENIUS, &norm));
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "|A| = %e, active field %d\n", norm, (int)active_field ));
  /* Set up solution */
  PetscCall(VecSet(subx, 0.0));
  /* Solve system */
  PetscCall(PetscLogStageRegister("Stage 1", &stage1));
  PetscCall(PetscLogStagePush(stage1));
  PetscCall(KSPCreate(PETSC_COMM_WORLD, &ksp));
  PetscCall(KSPSetOperators(ksp, subA, subA));
  PetscCall(KSPSetFromOptions(ksp));
  PetscCall(KSPSolve(ksp, subb, subx));
  PetscCall(PetscLogStagePop());
  /* Show result */
  PetscCall(VecDuplicate(subb, &u));
  PetscCall(MatMult(subA, subx, u));
  PetscCall(VecAXPY(u, -1.0, subb));
  PetscCall(VecNorm(u, NORM_2, &norm));
  PetscCall(VecNorm(subb, NORM_2, &normb));
  PetscCall(KSPGetIterationNumber(ksp, &its));
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Number of iterations = %3" PetscInt_FMT " N = %d\n", its, m));
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Residual norm %g\n", (double)norm/normb));
  /* Cleanup */
  PetscCall(KSPDestroy(&ksp));
  PetscCall(VecDestroy(&x));
  PetscCall(VecDestroy(&b));
  PetscCall(VecDestroy(&subx));
  PetscCall(VecDestroy(&subb));
  PetscCall(VecDestroy(&u));
  PetscCall(MatDestroy(&A));
  PetscCall(PetscFree3(idx[0], idx[1], idx[2]));
  PetscCall(PetscFree(idx[0]));
  PetscCall(PetscFree(idx[1]));
  PetscCall(PetscFree(idx[2]));
  for (int ii = 0 ; ii < num_fields ; ii++) {
    PetscCall(MatDestroy(&matSub[ii]));
    PetscCall(ISDestroy(&field[ii]));
  } 
  PetscCall(PetscFinalize());
  return 0;
}

/*TEST

    test:
      args: -ksp_gmres_cgs_refinement_type refine_always -f ${DATAFILESPATH}/matrices/arco1 -ksp_monitor_short
      requires: datafilespath double !complex !defined(PETSC_USE_64BIT_INDICES)

TEST*/
